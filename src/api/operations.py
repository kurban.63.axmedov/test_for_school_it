from typing import List, Optional
from fastapi import APIRouter, Depends

from src.models import ProfileGender, Profile, ProfileCreate
from src.services.operations import ProfileService

router = APIRouter(
    prefix='/profiles',
)


# @router.get('/hi')
# def test_get():
#     return {'message': 'Hello!'}


@router.get('/', response_model=List[Profile])
def get_profiles(
        gender: Optional[ProfileGender] = None,
        service: ProfileService = Depends(),
):
    return service.get_list(gender=gender)


@router.post('/', response_model=Profile)
def create_profile(
        profile_data: ProfileCreate,
        service: ProfileService = Depends(),
):
    return service.create(profile_data)
