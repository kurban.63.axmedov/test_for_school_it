from datetime import date
from decimal import Decimal
from enum import Enum
from typing import Optional
from pydantic import BaseModel


class ProfileGender(str, Enum):
    MALE = 'male'
    FEMALE = 'female'


class ProfileBase(BaseModel):
    name: Optional[str]
    gender: ProfileGender


class Profile(ProfileBase):
    id: int

    class Config:
        orm_mode = True


class ProfileCreate(ProfileBase):
    pass


class ProfileUpdate(ProfileBase):
    pass
