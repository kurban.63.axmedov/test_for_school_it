from typing import Optional, List

from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

from src import tables
from src.database import get_session
from src.models import ProfileCreate, ProfileGender


class ProfileService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get(self, profile_id: int) -> tables.Profile:
        profile = (
            self.session
            .query(tables.Profile)
            .filter_by(id=profile_id)
            .first()
        )
        if not profile:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    def get(self, profile_id: int) -> tables.Profile:
        return self._get(profile_id)

    def get_list(self, gender: Optional[ProfileGender] = None) -> List[tables.Profile]:
        query = self.session.query(tables.Profile)
        if gender:
            query = query.filter_by(gender=gender)
        profiles = query.all()
        return profiles

    def create(self, profile_data: ProfileCreate) -> tables.Profile:
        profile = tables.Profile(**profile_data.dict())
        self.session.add(profile)
        self.session.commit()
        return profile
