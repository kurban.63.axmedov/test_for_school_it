import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Profile(Base):
    __tablename__ = 'profiles'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    gender = sa.Column(sa.String)
