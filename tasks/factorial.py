n = 5
factorial = 1
for i in range(1, n + 1):
    factorial *= i
print(factorial)

# или так
import math

num = 5
print(math.factorial(num))
