from typing import List

lesson_from_leetcode = 'https://leetcode.com/problems/median-of-two-sorted-arrays/description/'
result_in_leetcode = 'https://leetcode.com/problems/median-of-two-sorted-arrays/submissions/899254224/'


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        sort_nums = sorted(nums1 + nums2)
        len_num = len(sort_nums)
        index2 = len_num // 2
        index1 = index2 - 1
        if len_num % 2 == 0:
            result = (sort_nums[index2] + sort_nums[index1]) / 2
            return float(result)
        else:
            result = sort_nums[index2]
            return float(result)


example = Solution()
result_example = example.findMedianSortedArrays(nums1=[1, 2, 6], nums2=[2])
print(result_example)
